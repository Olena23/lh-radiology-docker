## Docker containers for [LibreHealth Radiology](https://librehealth.io/projects/lh-radiology/)
[![pipeline status](https://gitlab.com/librehealth/radiology/lh-radiology-docker/badges/master/pipeline.svg)](https://gitlab.com/librehealth/radiology/lh-radiology-docker/commits/master)

The repository hosts [Docker Compose][] and resource files required to build [Docker][]
images and start containers that can be used by users and developers to test the
latest code in the repository. The [Docker][] container that is created uses standard
tomcat7:jre8 and mysql:5.7 images. It downloads the official releases the following modules:
 - radiology module (0.1.0-dev)
 - legacyui module (v1.4.0)
 - webservices.rest module (v2.17.0)
 - owa module (v1.8.0)

The initialized database includes 2.0 CIEL concepts from dropbox, along with the updated data from radiology acceptanceTests.

The tomcat:7-jre8 uses OpenJDK 8, which in turn uses Debian 8.1 (Jessie) base
image. Thus the lh-toolkit is deployed on tomcat7. The MySQL 5.7 image is used
for the database.

## Running the container

#### Requirements
If you are using Windows, please refer to our [README for Windows](README_windows.md).
Please install [Docker](https://docs.docker.com/) and
[Docker Compose](https://docs.docker.com/compose/install/) to create the
container images. You will need to pull the container image from the Gitlab Docker
Container Registry. Ensure that you do
not have any other servers running on 8080 (tomcat) and 3306 (mysql).

You will need to pull the container image from the Gitlab Docker Container Registry.

Ensure that you do not have any other servers running on 8080 (tomcat) and 3306 (mysql).

## Pulling the container image
Clone the project by typing the following command to the command line:

```
git clone https://gitlab.com/librehealth/radiology/lh-radiology-docker

```
![](images/clonning_repo.png)

Navigate to the directory where you cloned this project. Depending on how you want to interact with the container, run it in foreground or as a daemon.

To run the container in the foreground:
```
docker-compose -f docker-compose.dev.yml up
```
![](images/running_container_foreground.png)

MySQL will be started first and then lh-toolkit will be started on the containers.
When you are done using lh-toolkit you can press `Ctrl+C` to stop the container.

To run the container in the background:
```
docker-compose -f docker-compose.dev.yml up -d
```
![](images/running_container_background.png)


## Using lh-toolkit
To start using lh-toolkit, point your browser to localhost:8080/lh-toolkit .
The following are the authentication information:

* **User**: admin
* **Pass**: Admin123

![](images/login_screen.png)

![](images/welcome_screen.jpg)

## Bringing container down
To bring the container down and to free space on your machine run:
```
docker-compose down
```
![](images/bringing_container_down.png)

## Troubleshooting
When you are pulling the container image, the directory you are in does not matter.
However, if you try to run this docker image from outside of this project directory, you will get the following error:

![](images/running_image_from_wrong_dir.png)

Navigate to the project directory and enter the command again.

### Helpful links

[Docker Compose](https://docs.docker.com/compose/install)

[Docker](https://docs.docker.com/engine)
